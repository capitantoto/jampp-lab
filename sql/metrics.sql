WITH base AS ( 
     SELECT info AS keys 
     FROM estimators 
     WHERE info->>'to_sid' is not NULL 
     AND spec->>'label' = 'vta-carlos' 
     ORDER BY info->>'to_sid' DESC LIMIT 1 
 ), 
 extract AS ( 
     SELECT 
         b.scorer_keys AS scorer_key, 
         (a.keys->b.scorer_keys->'y'->>'n')::real AS weighted_n, 
         (a.keys->b.scorer_keys->'y'->>'mean')::real AS y_mean, 
         (a.keys->b.scorer_keys->'y_'->>'mean')::real AS y_est_mean, 
         (a.keys->b.scorer_keys->>'plr')::real AS plr, 
         (a.keys->b.scorer_keys->>'fscore')::real AS fscore, 
         (a.keys->b.scorer_keys->>'rrmspe')::real AS rrmspe 
     FROM base a 
     JOIN (SELECT scorer_keys FROM json_object_keys((SELECT (keys::json) FROM base)) scorer_keys) b 
     ON 1=1 
 ) 
 SELECT 
     scorer_key, 
     weighted_n, 
     y_mean, 
     y_est_mean, 
     plr, 
     fscore, 
     rrmspe
 FROM extract 
 WHERE y_mean > 0 
 ORDER BY 2 DESC;
