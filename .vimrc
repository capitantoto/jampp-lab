set nocompatible
set hidden
set history=1000
set tabstop=4 shiftwidth=4 softtabstop=4 shiftwidth=4
set expandtab
set hlsearch
set incsearch
set ignorecase
set smartcase
set autoindent
set textwidth=88
set formatoptions=tcroqlnj
set nojoinspaces
set nowrap
set mouse=a
set backspace=indent,eol,start
set whichwrap+=<,>,[,],h,l
set wildmenu
set ttimeout ttimeoutlen=100
set title titlestring=%t\ (%{$USER}@%{hostname()})
set ruler
set nonumber
set laststatus=1
set showcmd
set showmatch
set visualbell

filetype plugin indent on
syntax on

let mapleader = ' '
map <Leader><Leader> :ls<CR>:b<Space>

autocmd! BufRead,BufNewFile *.cfg set filetype=python
